//
//  DetailViewController.swift
//  SqliteFmdbDemo
//
//  Created by James Cash on 04-09-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var markTextField: UITextField!
    @IBOutlet weak var detailDescriptionLabel: UILabel!

    @IBOutlet var marksLabel: UILabel!
    
    
    @IBAction func saveMark(_ sender: Any) {
        let mark = Int(markTextField.text!)!
        db.addMark(mark: mark, forStudent: detailItem!)
        displayMarks()
    }

    func displayMarks() {
        let marks = db.marks(forStudent: detailItem!)
        marksLabel.text = "\(marks)"
    }
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                let fact = detail.funFact ?? "No Fact"
                label.text = "\(detail.name) - \(fact)"
                displayMarks()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: Student? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    var db: DatabaseManager!

}

