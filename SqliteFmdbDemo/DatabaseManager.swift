//
//  DatabaseManager.swift
//  SqliteFmdbDemo
//
//  Created by James Cash on 04-09-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import Foundation
import FMDB

struct Student {
    let name: String
    let funFact: String?
    let id: Int32
}

class DatabaseManager {
    private let db: FMDatabase

    init() {
        let dbURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("demo.db")
        print("Creating database in \(dbURL)")
        db = FMDatabase(url: dbURL)
        if !db.open() {
            print("Failed to open database!")
            abort()
        }
        createTables()
        let count = numberOfStudents()
        print("There are \(count) students in the database")
        if count == 0 {
            seedDatabase()
        }
    }

    deinit {
        db.close()
    }

    func createTables() {
        let createQuery = """
CREATE TABLE IF NOT EXISTS students (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name text NOT NULL,
  funFact text
);

CREATE TABLE IF NOT EXISTS marks (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  student_id INTEGER,
  mark INTEGER NOT NULL,
  FOREIGN KEY(student_id) REFERENCES students(id)
);
"""
        if !db.executeStatements(createQuery) {
            print("Failed to create tables!")
            abort()
        }
    }

    func seedDatabase() {
        let seedQuery = """
        INSERT INTO students (name, funFact) VALUES
        ("james", "highland games"),
        ("quux", "has a unique name"),
        ("foobar", "is annoyed by my variable names");
        """

        if !db.executeStatements(seedQuery) {
            print("Failed to seed students")
            abort()
        }
    }

    func numberOfStudents() -> Int32 {
        let query = "SELECT COUNT(*) AS count FROM students;"
        do {
            let resultSet = try db.executeQuery(query, values: nil)
            resultSet.next()
            return resultSet.int(forColumn: "count")
        } catch let err {
            print("Error fetching students count: \(err)")
            abort()
        }
    }

    func students() -> [Student] {
        do {
            let s = try db.executeQuery("SELECT * FROM students;", values: nil)
            var results = [Student]()
            while s.next() {
                results.append(
                    Student(name: s.string(forColumn: "name")!,
                            funFact: s.string(forColumn: "funFact"),
                            id: s.int(forColumn: "id")))
            }
            return results
        } catch let err {
            print("Error fetching students \(err)")
            abort()
        }
    }

    func addStudent(name: String, funFact: String?) {
//        let name = "); DELETE FROM students;--"
        let query = """
        INSERT INTO students (name, funFact)
        VALUES (:name, :funFact);
"""
        // by separating the query/update we want to run and the placeholder values, we are essentially passing those two things to the sqlite engine as distinct pieces of data, so then it knows, when it gets "name" that it should just be treated as data, not any sort of query to execute
        if !db.executeUpdate(query, withParameterDictionary:
            ["name": name,
             "funFact": funFact ?? NSNull()]) {
            print("Failed to add student")
            abort()
        }
    }

    func addMark(mark: Int, forStudent student: Student) {
        let query = """
        INSERT INTO marks (mark, student_id) VALUES
        (:mark, :student);
"""
        if !db.executeUpdate(
            query,
            withParameterDictionary: ["mark": mark,
                                      "student": student.id])
        {
            print("Failed to save mark \(mark) for \(student)")
            abort()
        }
    }

    func marks(forStudent student: Student) -> [Int32] {
        let query = "SELECT mark FROM marks WHERE student_id = :id"
        guard let rs = db.executeQuery(query, withParameterDictionary: ["id": student.id]) else {
            print("Error unwrapping query")
            abort()
        }
        var marks = [Int32]()
        while rs.next() {
            marks.append(rs.int(forColumn: "mark"))
        }
        return marks
    }

    func searchForStudents(search: String) -> [(Student, Double)] {
        let query = """
        SELECT students.id, students.name, students.funFact,
            -- avg is an aggregate function, because of the GROUP BY below
               avg(marks.mark) as average
        FROM students
        -- doing a LEFT join because we also want to get students that don't have any marks associated with them
        -- if we just do a plain JOIN it only gives us students that also have marks
        LEFT JOIN marks ON marks.student_id = students.id
         -- LIKE does wildcard matching, where "%" means "anything"
         -- and "||" is string concatenation
        WHERE students.name LIKE '%' || :search || '%'
        -- GROUP BY clause collapses multiple result rows with the same student.id together
        -- if we don't have this, then students that have multiple marks associated with them would appear multiple times in the results (one time for each mark that they have stored) (because that's how the join works
        -- this is why we need the `avg` aggregate function above, because if we want to group by student id, then we need some way of collapsing the multiple results for marks together
        GROUP BY students.id;
"""
        guard let rs = db.executeQuery(query, withParameterDictionary: ["search": search]) else {
            print("Failed to run search query")
            abort()
        }
        var results = [(Student, Double)]()
        while rs.next() {
            let student = Student(name: rs.string(forColumn: "name")!,
                                  funFact: rs.string(forColumn: "funFact"),
                                  id: rs.int(forColumn: "id"))
            let avg = rs.double(forColumn: "average")
            results.append((student, avg))
        }
        return results
    }
}
